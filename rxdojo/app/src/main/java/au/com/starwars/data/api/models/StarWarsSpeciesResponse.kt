package au.com.starwars.data.api.models

import com.google.gson.annotations.SerializedName

data class StarWarsSpeciesResponse(@SerializedName("language") val language: String)