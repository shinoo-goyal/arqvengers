package au.com.starwars.injection.components

import au.com.starwars.Application
import au.com.starwars.injection.modules.ApplicationModule
import au.com.starwars.injection.modules.NetworkModule
import au.com.starwars.injection.modules.SingletonModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by azfar.siddiqui on 7/9/17.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class,
        SingletonModule::class,
        NetworkModule::class))
interface ApplicationComponent : Singletons {

    fun inject(application: Application)
}