package au.com.starwars.injection.modules

import au.com.starwars.data.LocalProperties
import au.com.starwars.data.api.services.StarWarsService
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by azfar.siddiqui on 19/9/17.
 */
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttp(localProperties: LocalProperties): OkHttpClient {
        val builder = OkHttpClient().newBuilder()
                .connectTimeout(localProperties.apiConnectTimeout, TimeUnit.SECONDS)
                .readTimeout(localProperties.apiReadTimeout, TimeUnit.SECONDS)
                .writeTimeout(localProperties.apiWriteTimeout, TimeUnit.SECONDS)

        return builder.build()
    }


    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient,
                        localProperties: LocalProperties): Retrofit = buildRetrofit(okHttpClient, localProperties)

    private fun buildRetrofit(okHttpClient: OkHttpClient,
                              localProperties: LocalProperties): Retrofit {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(localProperties.apiBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }

    @Singleton
    @Provides
    fun provideStarWarsService(retrofit: Retrofit): StarWarsService {
        return retrofit.create(StarWarsService::class.java)
    }
}