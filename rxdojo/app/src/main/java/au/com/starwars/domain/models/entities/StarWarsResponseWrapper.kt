package au.com.starwars.domain.models.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class StarWarsResponseWrapper(val characters: List<StarWarsCharacter>) : Parcelable

@Parcelize
class StarWarsCharacter(val name: String,
                        val birthYear: String,
                        val species: String) : Parcelable
