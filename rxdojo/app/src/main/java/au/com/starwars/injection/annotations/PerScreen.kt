package au.com.starwars.injection.annotations

import javax.inject.Scope

/**
 * Created by azfar.siddiqui on 7/9/17.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerScreen
