package au.com.starwars.data.mappers

import au.com.starwars.data.api.models.StarWarsResult
import au.com.starwars.domain.models.entities.StarWarsCharacter
import au.com.starwars.domain.models.entities.StarWarsResponseWrapper

class StarWarsMapper {
    companion object {
        fun mapToStarWarsMapper(characterList: List<StarWarsResult>) =
                StarWarsResponseWrapper(characterList.map {
                    StarWarsCharacter(it.name, it.birthYear, it.species.get(0))
                })
    }
}