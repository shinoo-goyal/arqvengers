package au.com.starwars.data

import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by azfar.siddiqui on 25/9/17.
 */
@Singleton
class LocalProperties @Inject constructor() {

    class ApiUrlPath {
        companion object {
            const val GET_STARWARS_LIST = "people"
        }
    }

    val apiBaseUrl = "https://swapi.co/api/"
    val apiConnectTimeout: Long = 3
    val apiReadTimeout: Long = 3
    val apiWriteTimeout: Long = 3
}