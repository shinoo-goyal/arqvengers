package au.com.starwars.domain.repositories

import au.com.starwars.domain.models.entities.StarWarsResponseWrapper
import io.reactivex.Single

/**
 * Created by shinoo.goyal on 18/10/17.
 */
interface StarWarsRepository {
    fun getStarWarsCharacterList(): Single<StarWarsResponseWrapper>
    fun getStarWarsSpecies(url: String): Single<String>

}