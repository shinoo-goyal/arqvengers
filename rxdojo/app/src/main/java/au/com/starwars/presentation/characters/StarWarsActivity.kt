package au.com.starwars.presentation.characters

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import au.com.starwars.Application
import au.com.starwars.R
import au.com.starwars.domain.models.entities.StarWarsCharacter
import au.com.starwars.injection.components.ApplicationComponent
import au.com.starwars.injection.components.DaggerActivityComponent
import au.com.starwars.injection.modules.ActivityModule
import au.com.starwars.presentation.common.BaseActivity
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class StarWarsActivity : BaseActivity<StarWarsPresenter>(), StarWarsPresenter.ViewSurface {

    private lateinit var listAdapter: StarWarsAdapter
    private var listCharacters = PublishSubject.create<List<StarWarsCharacter>>()
    private var dataObservable: Observable<List<StarWarsCharacter>> = listCharacters as Observable<List<StarWarsCharacter>>
    protected var subscriptionsWhileActive: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onCreate(this)
        setUpRecyclerView()
        setUpClickListeners()
    }

    override fun onResume() {
        super.onResume()
        presenter.getStarWarsList()
    }

    override fun onPause() {
        super.onPause()
        subscriptionsWhileActive.clear()
    }

    fun getAppComponent(): ApplicationComponent = (application as Application).applicationComponent

    override fun inject() {
        return DaggerActivityComponent.builder()
                .applicationComponent(getAppComponent())
                .activityModule(ActivityModule(this))
                .build()
                .inject(this)
    }

    override fun showStarWarsCharacterList(characters: List<StarWarsCharacter>) {
        listCharacters.onNext(characters)
    }

    override fun showStarWarsPopUp(language: String) {
        val alertDialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(language)
                .setPositiveButton("OK", null)
        alertDialogBuilder.show()
    }

    override fun showErrorPopup() {
        val alertDialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage("This is an error")
                .setPositiveButton("OK", { _, _ -> presenter.getStarWarsList() })
        alertDialogBuilder.show()
    }

    private fun setUpRecyclerView() {
        listAdapter = StarWarsAdapter({ item: StarWarsCharacter -> onStarWarsCharacterClicked(item) })

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        list_characters.layoutManager = layoutManager
        list_characters.setHasFixedSize(true)
        list_characters.adapter = listAdapter
    }

    private fun onStarWarsCharacterClicked(character: StarWarsCharacter) {
        presenter.loadSpeciesFromUrl(character.species)
    }

    private fun setUpClickListeners() {
        val searchObservable: Observable<CharSequence> = RxTextView.textChanges(edt_search)
        with(subscriptionsWhileActive) {
            add(io.reactivex.Observable.combineLatest(searchObservable, dataObservable, io.reactivex.functions.BiFunction { searchText: CharSequence, characters: List<StarWarsCharacter> ->
                if (searchText.trim().isNotEmpty() && characters.isNotEmpty()) {
                    listAdapter.setStarWarsList(characters.filter { it.name.contains(searchText.toString(), true) })
                } else {
                    listAdapter.setStarWarsList(characters)
                }
            }).debounce(2000, TimeUnit.MILLISECONDS).subscribe())
        }
    }
}
