package au.com.starwars.injection.modules

import android.app.Activity
import android.content.Context
import android.support.v7.app.AppCompatActivity
import au.com.starwars.injection.annotations.ActivityContext
import dagger.Module
import dagger.Provides

/**
 * Created by azfar.siddiqui on 7/9/17.
 */
@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    fun provideAppCompatActivity(): AppCompatActivity = activity

    @Provides
    fun provideActivity(): Activity = activity

    @Provides
    @ActivityContext
    fun provideContext(): Context = activity

}
