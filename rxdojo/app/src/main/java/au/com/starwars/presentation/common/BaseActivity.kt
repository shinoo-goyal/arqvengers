package au.com.starwars.presentation.common

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by azfar.siddiqui on 11/10/17.
 */
abstract class BaseActivity<PresenterType : BasePresenter> : AppCompatActivity() {


    @Inject
    lateinit var presenter: PresenterType

    protected var subscriptionsWhileVisible: CompositeDisposable = CompositeDisposable()
    protected var subscriptionsWhileInMemory: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }

    abstract fun inject()

    override fun onPause() {
        presenter.onViewInactive()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        presenter.onViewActive()
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewShown()
    }

    override fun onStop() {
        stopObservingEvents()
        presenter.onViewHidden()
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDestroyed()
        subscriptionsWhileInMemory.clear()
    }


    private fun stopObservingEvents() {
        subscriptionsWhileVisible.clear()
    }
}