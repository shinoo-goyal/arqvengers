package au.com.starwars.data.api.models

import com.google.gson.annotations.SerializedName

/**
 * Created by shinoo.goyal on 19/10/17.
 */
data class StarWarsResponse(@SerializedName("results") val results: List<StarWarsResult>)

data class StarWarsResult(@SerializedName("name") val name: String,
                          @SerializedName("birth_year") val birthYear: String,
                          @SerializedName("species") val species: List<String>)