package au.com.starwars.data.managers

import au.com.starwars.data.api.CallCoordinator
import au.com.starwars.data.api.services.StarWarsService
import au.com.starwars.data.mappers.StarWarsMapper
import au.com.starwars.domain.models.entities.StarWarsResponseWrapper
import au.com.starwars.domain.repositories.StarWarsRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by shinoo.goyal on 18/10/17.
 */
@Singleton
class StarWarsManager @Inject constructor(private val starWarsService: StarWarsService) : StarWarsRepository {
    override fun getStarWarsCharacterList(): Single<StarWarsResponseWrapper> {
        val callCoordinator = CallCoordinator()
        return Single.fromCallable({
            val response = callCoordinator.execute(starWarsService.getStarWarsCharacterList())
            StarWarsMapper.mapToStarWarsMapper(response.results)
        })
    }

    override fun getStarWarsSpecies(url: String): Single<String> {
        val callCoordinator = CallCoordinator()
        return Single.fromCallable({
            val response = callCoordinator.execute(starWarsService.getStarWarsSpecies(url))
            response.language
        })
    }
}