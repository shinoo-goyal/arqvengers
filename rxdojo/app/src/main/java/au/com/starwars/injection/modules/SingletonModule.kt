package au.com.starwars.injection.modules

import au.com.starwars.data.managers.StarWarsManager
import au.com.starwars.domain.repositories.StarWarsRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface SingletonModule {
    @Binds
    @Singleton
    fun bindStarWarsRepository(starWarsManager: StarWarsManager): StarWarsRepository
}