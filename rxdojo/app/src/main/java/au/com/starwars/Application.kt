package au.com.starwars

import android.app.Application
import au.com.starwars.injection.components.ApplicationComponent
import au.com.starwars.injection.components.DaggerApplicationComponent
import au.com.starwars.injection.modules.ApplicationModule
import javax.inject.Inject

/**
 * Created by azfar.siddiqui on 7/9/17.
 */
class Application : Application() {

    @Inject
    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        initApplicationComponent()
    }

    private fun initApplicationComponent() {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build().inject(this)
    }
}