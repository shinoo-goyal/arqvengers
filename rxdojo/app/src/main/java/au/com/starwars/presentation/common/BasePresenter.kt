package au.com.starwars.presentation.common

import io.reactivex.disposables.CompositeDisposable

open class BasePresenter {

    protected val tasksWhileInMemory = CompositeDisposable()
    protected val tasksWhileVisible = CompositeDisposable()
    protected val tasksWhileActive = CompositeDisposable()


    open fun onViewShown() {}

    open fun onViewActive() {}

    open fun onViewInactive() {
        tasksWhileActive.clear()
    }

    open fun onViewHidden() {
        tasksWhileVisible.clear()
    }

    open fun onViewDestroyed() {
        tasksWhileActive.clear()
        tasksWhileVisible.clear()
        tasksWhileInMemory.clear()
    }

}