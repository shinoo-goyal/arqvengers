package au.com.starwars.domain.exceptions

/**
 * Created by azfar.siddiqui on 15/11/17.
 */
open class BaseException : Exception()