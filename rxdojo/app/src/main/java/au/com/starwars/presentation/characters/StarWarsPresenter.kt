package au.com.starwars.presentation.characters

import au.com.starwars.domain.models.entities.StarWarsCharacter
import au.com.starwars.domain.usecases.starwars.GetStarWarsCharactersListUseCase
import au.com.starwars.domain.usecases.starwars.GetStarWarsSpeciesUseCase
import au.com.starwars.presentation.common.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class StarWarsPresenter @Inject constructor(private val getStarWarsCharactersListUseCase: GetStarWarsCharactersListUseCase,
                                            private val getStarWarsSpeciesUseCase: GetStarWarsSpeciesUseCase) : BasePresenter() {

    private lateinit var view: ViewSurface


    fun onCreate(view: ViewSurface) {
        this.view = view
    }

    fun getStarWarsList() {
        tasksWhileInMemory.add(getStarWarsCharactersListUseCase.execute()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.showStarWarsCharacterList(it.characters)
                }, {
                    view.showErrorPopup()
                }
                ))
    }

    fun loadSpeciesFromUrl(url: String) {
        tasksWhileInMemory.add(getStarWarsSpeciesUseCase.execute(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.showStarWarsPopUp(it)
                }, {
                    view.showErrorPopup()
                }
                ))
    }

    interface ViewSurface {
        fun showStarWarsCharacterList(characters: List<StarWarsCharacter>)
        fun showStarWarsPopUp(language: String)
        fun showErrorPopup()
    }
}
