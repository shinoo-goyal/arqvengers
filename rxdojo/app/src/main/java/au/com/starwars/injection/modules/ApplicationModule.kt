package au.com.starwars.injection.modules

import android.app.Application
import android.content.Context
import au.com.starwars.injection.annotations.AppContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by azfar.siddiqui on 7/9/17.
 */
@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    @AppContext
    fun provideContext(): Context = application
}