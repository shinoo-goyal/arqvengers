package au.com.starwars.injection.components

import android.content.Context
import au.com.starwars.data.LocalProperties
import au.com.starwars.domain.repositories.StarWarsRepository
import au.com.starwars.injection.annotations.AppContext

interface Singletons {
    @AppContext
    fun provideContext(): Context
    fun provideLocalProperties(): LocalProperties
    fun provideStarWarsRepository(): StarWarsRepository
}