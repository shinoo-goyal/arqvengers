package au.com.starwars.domain.exceptions

/**
 * Created by azfar.siddiqui on 28/9/17.
 */
open class ApplicationException(val errorCode: String) : BaseException()