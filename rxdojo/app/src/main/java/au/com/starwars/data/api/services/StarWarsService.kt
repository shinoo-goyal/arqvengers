package au.com.starwars.data.api.services

import au.com.starwars.data.LocalProperties
import au.com.starwars.data.api.models.StarWarsResponse
import au.com.starwars.data.api.models.StarWarsSpeciesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by shinoo.goyal on 19/10/17.
 */
interface StarWarsService {

    @GET(LocalProperties.ApiUrlPath.GET_STARWARS_LIST)
    fun getStarWarsCharacterList(): Call<StarWarsResponse>

    @GET
    fun getStarWarsSpecies(@Url url: String): Call<StarWarsSpeciesResponse>

}
