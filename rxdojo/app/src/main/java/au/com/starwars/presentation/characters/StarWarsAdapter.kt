package au.com.starwars.presentation.characters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import au.com.starwars.R
import au.com.starwars.domain.models.entities.StarWarsCharacter
import kotlinx.android.synthetic.main.layout_star_wars_item.view.*

class StarWarsAdapter(private val clickListener: (StarWarsCharacter) -> Unit) : RecyclerView.Adapter<StarWarsAdapter.StarWarsItemViewHolder>() {

    private var starWarsList: List<StarWarsCharacter> = emptyList()

    override fun getItemCount() = starWarsList.size

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): StarWarsItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_star_wars_item, parent, false)
        return StarWarsItemViewHolder(view)
    }

    fun setStarWarsList(list: List<StarWarsCharacter>) {
        this.starWarsList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: StarWarsItemViewHolder, position: Int) {
        holder.bindData(starWarsList[position], clickListener)
    }

    class StarWarsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(starWarsCharacter: StarWarsCharacter, listener: (StarWarsCharacter) -> Unit) {
            itemView.txt_name.text = starWarsCharacter.name
            itemView.setOnClickListener { listener(starWarsCharacter) }
        }
    }
}

