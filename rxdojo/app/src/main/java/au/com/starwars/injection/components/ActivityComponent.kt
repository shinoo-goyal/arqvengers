package au.com.starwars.injection.components

import android.content.Context
import android.support.v7.app.AppCompatActivity
import au.com.starwars.injection.annotations.ActivityContext
import au.com.starwars.injection.annotations.PerScreen
import au.com.starwars.injection.modules.ActivityModule
import au.com.starwars.presentation.characters.StarWarsActivity
import dagger.Component

/**
 * Created by azfar.siddiqui on 7/9/17.
 */
@PerScreen
@Component(dependencies = arrayOf(ApplicationComponent::class),
        modules = arrayOf(ActivityModule::class))
interface ActivityComponent : Singletons {

    fun provideAppCompatActivity() : AppCompatActivity

    @ActivityContext
    fun provideActivityContext() : Context

    fun inject(activity: StarWarsActivity)
}