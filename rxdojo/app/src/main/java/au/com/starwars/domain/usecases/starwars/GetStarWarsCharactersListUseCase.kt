package au.com.starwars.domain.usecases.starwars

import au.com.starwars.domain.models.entities.StarWarsResponseWrapper
import au.com.starwars.domain.repositories.StarWarsRepository
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by shinoo.goyal on 18/10/17.
 */
class GetStarWarsCharactersListUseCase @Inject constructor(private val starWarsRepository: StarWarsRepository) {

    fun execute(): Single<StarWarsResponseWrapper> = starWarsRepository.getStarWarsCharacterList().delay(2000, TimeUnit.MILLISECONDS)
}

