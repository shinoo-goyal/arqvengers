package au.com.starwars.domain.usecases.starwars

import au.com.starwars.domain.models.entities.StarWarsResponseWrapper
import au.com.starwars.domain.repositories.StarWarsRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by shinoo.goyal on 18/10/17.
 */
class GetStarWarsSpeciesUseCase @Inject constructor(private val starWarsRepository: StarWarsRepository) {

    fun execute(url: String): Single<String> = starWarsRepository.getStarWarsSpecies(url)
}

